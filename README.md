# Testing

This is a simple unit testing framework for C++. It supports assertions on
any type that supports the == operator, with additional utilities such as
asserting that containers match, or that floating-point values match within some
imprecision.

This project was heavily inspired by the unit testing framework used by the EECS
280 course at the University of Michigan! I assure you all the code here is
original, but most of the ideas seen here are my own take on their featureset.

Example use:
```cpp
#include <array>

// Including the library.
#include "Testing.h"
using namespace test;

// Test declaration.
TEST(test_comparisons) {
    // Checks if an expression is true.
    ASSERT_TRUE(1 == 1);
    
    // Checks if an expression is false.
    ASSERT_FALSE(1 == 2);
    
    // Checks if two expressions are equal. Gives more specific feedback than
    // ASSERT_TRUE.
    ASSERT_EQUAL(1, 1);
    
    // Checks if two expressions are not equal.
    ASSERT_INEQUAL(1, 2);
    
    // Checks if two values are equal within some imprecision.
    ASSERT_ALMOST_EQUAL(1, 1.1, 0.2);
    
    // Checks if two containers are equal by iterating through both and
    // comparing each element.
    ASSERT_CONTAINERS_EQUAL(std::array<int, 3>{ 1, 2, 3 }, 
                            std::array<int, 3>{ 1, 2, 3 });
}

// Executes test cases.
RUN();