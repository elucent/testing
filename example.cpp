#include "Testing.h"

#include <string>
#include <vector>

using namespace std;
using namespace test;

TEST(basic_comparisons) {
    ASSERT_EQUAL(2, 2);
    ASSERT_INEQUAL(1, 2);

    ASSERT_EQUAL('a', 'c');
}

TEST(compound_comparisons) {
    string a = "hello";
    ASSERT_EQUAL(a, "hello");

    array<int, 4> arr1 = { 1, 2, 3, 4 }, arr2 = { 1, 2, 3, 4 };
    ASSERT_TRUE(arr1 == arr2);

    arr1[0] = 4;
    ASSERT_TRUE(arr1 < arr2);
}

TEST(booleans) {
    ASSERT_TRUE(true);
    ASSERT_FALSE(false);

    ASSERT_TRUE(false);
}

TEST(imprecision) {
    ASSERT_ALMOST_EQUAL(0.1, 0.100000000001, 0.000001);
    ASSERT_ALMOST_EQUAL(4.1, 4, 0.1);

    ASSERT_ALMOST_EQUAL(3.1f, 3.0f, .05f);
}

TEST(containers) {
    vector<int> a = { 1, 2, 3, 4 };
    vector<int> b;
    b.push_back(1);
    b.push_back(2);
    b.push_back(3);
    b.push_back(4);
    ASSERT_CONTENTS_EQUAL(a, b);

    b.push_back(5);
    ASSERT_CONTENTS_EQUAL(a, b);
}

RUN();