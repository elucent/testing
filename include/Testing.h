#ifndef TESTING_H
#define TESTING_H

#include <string>
#include <map>
#include <iostream>
#include <cmath>

#define TEST(fn) void fn (const std::string& name); \
                   auto fn##_inserted = (test::__TEST_FUNCS[#fn] = fn); \
                   void fn (const std::string& name) 

#define RUN() bool __test_result = test::__TEST_ALL(); \
              int main() { return __test_result ? 0 : 1; }

#define ASSERT_TRUE(a) _ASSERT_TRUE((a))
#define ASSERT_FALSE(a) _ASSERT_FALSE((a))
#define ASSERT_EQUAL(a, b) _ASSERT_EQUAL((a), (b))
#define ASSERT_INEQUAL(a, b) _ASSERT_INEQUAL((a), (b))
#define ASSERT_CONTENTS_EQUAL(a, b) _ASSERT_CONTENTS_EQUAL((a), (b))
#define ASSERT_ALMOST_EQUAL(a, b, precision) _ASSERT_ALMOST_EQUAL((a), (b), (precision))
#define FAIL(a) _FAIL((a));

#define _ASSERT_TRUE(a) { if (!(a)) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' is false!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } }

#define _ASSERT_FALSE(a) { if (a) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' is true!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } }

#define _ASSERT_EQUAL(a, b) { if ((a) != (b)) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' != '" << b << "'!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } }

#define _ASSERT_INEQUAL(a, b) { if ((a) == (b)) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' == '" << b << "'!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } }

#define _ASSERT_CONTENTS_EQUAL(a, b) { auto i = a.begin(); auto j = b.begin(); \
                    while (i != a.end() && j != b.end()) if (*i != *j) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << *i << "' != '" << *j << "'!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } else ++i, ++j; \
                    if ((i != a.end()) != (j != b.end())) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": Size mismatch, '" << a.size() << "' != '" << b.size() << "'!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    } }

#define _ASSERT_ALMOST_EQUAL(a, b, precision) if (((a)-(b) < 0 ? (b)-(a) : (a)-(b)) > precision) { \
                        std::cout << "[FAILURE] Line " << __LINE__ << ": '" << a << "' != '" << b << "'!" << std::endl; \
                        test::__TEST_RESULTS[name] = false; \
                        return; \
                    }

#define _FAIL(a) std::cout << "[FAILURE] Line " << __LINE__ << ": " << a << std::endl; \
                 test::__TEST_RESULTS[name] = false; \
                 return;

namespace test {
    using __TESTFN = void (*)(const std::string&);

    extern std::map<std::string, __TESTFN> __TEST_FUNCS;
    extern std::map<std::string, bool> __TEST_RESULTS;

    bool __TEST_ALL();
}

#endif
